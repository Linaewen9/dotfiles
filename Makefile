# {{{ Global targets

.PHONY: all

all:	general	\
	scripts	\
	shell		\
	X		\
	emacs		\
	wm		\
	git		\
	compton	\
	polybar	\
	wiki		\
	sakura		\
	sxhkd

clean:	general_clean		\
	scripts_clean		\
	shell_clean		\
	X_clean		\
	emacs_clean		\
	wm_clean		\
	git_clean		\
	compton_clean		\
	polybar_clean		\
	wiki_clean		\
	sakura_clean		\
	sxhkd_clean

# }}}
# {{{ General

general: ~/.dotfiles

general_clean:
	rm ~/.dotfiles

~/.dotfiles:
	ln -s $(CURDIR) ~/.dotfiles

# }}}
# {{{ Scripts

scripts: ~/.scripts

scripts_clean:
	rm ~/.scripts

~/.scripts:
	ln -s $(CURDIR)/scripts ~/.scripts

# }}}
# {{{ Shell

SHELLDIR=$(CURDIR)/shell

shell: bash zsh

shell_clean: bash_clean zsh_clean

# {{{ Bash

bash: ~/.bashrc

bash_clean:
	rm ~/.bashrc

~/.bashrc:
	cp $(SHELLDIR)/default_bashrc ~/.bashrc

# }}}
# {{{ Zsh

zsh: ~/.zshrc

zsh_clean:
	rm ~/.zshrc

~/.zshrc:
	cp $(SHELLDIR)/default_zshrc ~/.zshrc

# }}}

# }}}
# {{{ X

X: ~/.Xresources

X_clean:
	rm ~/.Xresources

~/.Xresources:
	ln -s $(CURDIR)/Xresources ~/.Xresources

# }}}
# {{{ Emacs

emacs: ~/.emacs.d
	make -C ~/.emacs.d

emacs_clean:
	make -C ~/.emacs.d clean
	rm ~/.emacs.d

~/.emacs.d:
	ln -fs $(CURDIR)/emacs.d ~/.emacs.d

# }}}
# {{{ WM

WMDIR=$(CURDIR)/wm

wm: i3

wm_clean: i3_clean

# {{{ i3

i3: ~/.i3

i3_clean:
	rm ~/.i3

~/.i3:
	ln -s $(WMDIR)/i3 ~/.i3

# }}}

# }}}
# {{{ Git

git: ~/.gitconfig

git_clean:
	rm ~/.gitconfig

~/.gitconfig:
	ln -s $(CURDIR)/git/gitconfig ~/.gitconfig

# }}}
# {{{ Compton

compton: ~/.compton.conf

compton_clean:
	rm ~/.compton.conf

~/.compton.conf:
	ln -s $(CURDIR)/compton.conf ~/.compton.conf

# }}}
# {{{ Polybar

polybar: ~/.config/polybar/config

polybar_clean:
	rm ~/.config/polybar/config

~/.config/polybar/config:
	mkdir -p ~/.config/polybar
	ln -s $(CURDIR)/config/polybar/config ~/.config/polybar/config

# }}}
# {{{ Wiki

wiki_DIRBASE=~/.wiki
wiki_DIRHTML=~/.wiki/html
wiki_BATCH=~/.emacs.d/batch.el

wiki: ~/.wiki
	rm -f ./.tmp.batch.el
	sed 's#(setq dir-base "/tmp")#(setq dir-base "'$(wiki_DIRBASE)'")#g;s#(setq dir-html "/tmp")#(setq dir-html "'$(wiki_DIRHTML)'")#g' < $(wiki_BATCH) > ./.tmp.batch.el
	cat ./.tmp.batch.el
	emacs --batch -l ./.tmp.batch.el -f org-publish-all
	rm -f ./.tmp.batch.el

~/.wiki:
	ln -s $(CURDIR)/wiki ~/.wiki
	mkdir -p ~/.wiki/html

wiki_clean:
	find $(wiki_DIRHTML) -name "*.html" -delete -print
	rm ~/.wiki

# }}}
# {{{ Go

go:
	go get golang.org/x/tools/cmd/...
	go get github.com/rogpeppe/godef
	go get -u github.com/nsf/gocode

# }}}
# {{{ Sakura

sakura: ~/.config/sakura/sakura.conf

sakura_clean:
	rm ~/.config/sakura/sakura.conf

~/.config/sakura/sakura.conf:
	mkdir -p ~/.config/sakura
	ln -s $(CURDIR)/config/sakura/sakura.conf ~/.config/sakura/sakura.conf

# }}}
# {{{ Sxhkd

sxhkd: ~/.config/systemd/user/sxhkd.service

sxhkd_clean:
	rm ~/.config/systemd/user/sxhkd.service

~/.config/systemd/user/sxhkd.service:
	mkdir -p ~/.config/systemd/user
	ln -s $(CURDIR)/config/systemd/user/sxhkd.service ~/.config/systemd/user/sxhkd.service

# }}}
