origin_branch=$1
new_branch=$2

if [ $# -lt 2 ]
then
    echo "No enough parameters"
    exit 1
fi

origin_over=$(git log master..$origin_branch --oneline | wc -l)
new_over=$(git log master..$new_branch --oneline | wc -l)
oc=$(($origin_over - 1))
nc=$(($new_over - 1))

# This is a .txt because github doesn't accept .diff
output=/tmp/gen_diff.diff.txt
rm -f $output
while [ $oc != -1 -a $nc != -1 ]
do
    git log $new_branch~$nc -1 --oneline
    git log $new_branch~$nc -1 >> $output
    git diff $origin_branch~$oc $new_branch~$nc >> $output
    echo "##########" >> $output

    oc=$(($oc-1))
    nc=$(($nc-1))
done
