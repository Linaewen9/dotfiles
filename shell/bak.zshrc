# -*-mode: shell-script; coding: utf-8 -*-

# {{{ Options

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=8000
bindkey -e
export LANG="en_US.UTF-8"
. ~/.dotfiles/shell/bash_aliases
export EDITOR="emacsclient -t"



# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit promptinit colors zcalc tetris vcs_info
colors
compinit
promptinit;

autoload -U zutil
autoload -U complist

# next lets set some enviromental/shell pref stuff up
# setopt NOHUP
#setopt NOTIFY
#setopt NO_FLOW_CONTROL
setopt NO_EQUALS
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
unsetopt BG_NICE        # do NOT nice bg commands
setopt CORRECT          # command CORRECTION
setopt EXTENDED_HISTORY # puts timestamps in the history
# setopt HASH_CMDS# turns on hashing
#
setopt MENUCOMPLETE
setopt ALL_EXPORT
# Set/unset  shell options
setopt   notify globdots correct pushdtohome cdablevars autolist
setopt   autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
unsetopt bgnice autoparamslash
# turn off correction for proper commands
unsetopt correctall

# Autoload zsh modules when they are referenced
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
######## zmodload -ap zsh/mapfile mapfile


# EDITOR

export PATH=/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH

# GNU utils path
export PATH=/opt/local/libexec/gnubin/:$PATH

setopt prompt_subst

if [ "$TERM" != "dumb" ]; then
    alias ls='ls --color=auto'
fi
# }}}
# {{{ Prompt

zstyle ':vcs_info:*' enable git svn cvs hg bzr darcs
zstyle ':vcs_info:git*:*' get-revision true
zstyle ':vcs_info:git*:*' check-for-changes true
zstyle ':vcs_info:git*' formats "%{$fg_bold[grey]%}%8.8i %b \
%{$fg_bold[yellow]%}%c%u%m"
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked git-stash

+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
     git status --porcelain | grep -m 1 '^??' &>/dev/null
  then
    hook_com[misc]='?'
  fi
}

function +vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        hook_com[misc]+='$'
    fi
}

vcs_info_wrapper() {
    vcs_info
    if [ -n "$vcs_info_msg_0_" ]; then
        echo "${vcs_info_msg_0_}%{$reset_color%}"
    fi
}

# Prompt
if [[ $EUID == 0 ]]
then
    PROMPT="%{$fg_bold[grey]%}%* \
%{$fg_bold[red]%}%n%{$fg_bold[grey]%}@%{$fg_bold[cyan]%}%m \
%{$fg_bold[white]%}%~ \
"'$(vcs_info_wrapper)'"
%{$fg_bold[blue]%}>%{$reset_color%} "  # root dir #
else
    PROMPT="%{$fg_bold[grey]%}%* \
%{$fg_bold[blue]%}%n%{$fg_bold[grey]%}@%{$fg_bold[cyan]%}%m \
%{$fg_bold[white]%}%~ \
"'$(vcs_info_wrapper)'"
%{$fg_bold[blue]%}>%{$reset_color%} "  # user dir #
fi

# }}}
# {{{ Term title
# Set xterm title
case $TERM in (xterm*|*rxvt*)
precmd () { print -Pn "\e]0;%n@%m: %~\a" }
preexec () { print -Pn "\e]0;%n@%m: $1\a" }
;;
esac

# allow emacs to track your current directory as you cd around
if [ -n "$INSIDE_EMACS" ]; then
    chpwd() { print -P "\033AnSiTc %d" }
    print -P "\033AnSiTu %n"
    print -P "\033AnSiTc %d"
fi


# EMACS compatibility
if [ "$EMACS" ]; then
    unsetopt zle
    export TERM=xterm-color
fi

# }}}
# {{{ Completion

zstyle ':completion:*:default' list-colors "${(s.:.)LS_COLORS}"

zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

# Completion Styles

# list of completers to use
# zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'

# insert all expansions for expand completer
# zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

## add colors to processes for kill completion
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

zstyle ':completion:*:*:kill:*:processes' command 'ps --forest -A -o pid,user,cmd'
zstyle ':completion:*:processes-names' command 'ps axho command'

zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show

# }}}
# {{{ Custom commands

# Clean old docker containers
docker-cleanup () {
    echo "* Removing old containers"
    docker ps -qa | xargs --no-run-if-empty -n 1 docker rm
    echo "* Removing test (untagged) images"
    docker images | grep --color=auto '<none>' | awk '{print $3}' | xargs --no-run-if-empty -n 1 docker rmi
}

randbug () {
    local i=1;
    while $@; do
        echo "---------- randbug: run #$i done ----------";
        i=$((i+1));
    done
    echo "randbug: failure occurred at run #$i";
}

# }}}

source ~/.dotfiles/shell/fzfconfig
