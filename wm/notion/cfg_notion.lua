-- Notion main config file

-------------------
-- cfg_keyboard.lua
META="Mod4+"
ALTMETA=""
META_KEY="X"

-- use azerty
os.execute("setxkbmap us")
os.execute("setxkbmap -option caps:super")
os.execute("setxkbmap -option compose:rwin")
os.execute("xset r rate 400 60")
os.execute("xrdb -merge ~/.Xresources")
os.execute("dunst -config ~/.i3/dunst/dunstrc&")
os.execute("if [[ $(hostname) = dracula ]]; then " ..
              "echo dracula > /tmp/notion_info " ..
              "&& xrandr --output DVI-D-0 --primary --auto " ..
              "--output HDMI-0 --left-of DVI-D-0 " ..
              "&& xset m 1/6;" ..
           "elif [[ $(hostname) = alucard ]]; then " ..
              "echo alucard > /tmp/notion_info;" ..
           "else " ..
              "echo meh > /tmp/notion_info;" ..
              "xrandr --output HDMI2 --primary --left-of HDMI1;" ..
              "fi")

-------------------
-- cfg_settings.lua

-- Terminal emulator
XTERM="urxvt"

-- Some basic settings
ioncore.set{
    -- Maximum delay between clicks in milliseconds to be considered a
    -- double click.
    dblclick_delay=200,

    -- Opaque resize?
    --opaque_resize=false,

    -- Movement commands warp the pointer to frames instead of just
    -- changing focus. Enabled by default.
    warp=false,

    -- Switch frames to display newly mapped windows
    switchto=true,

    -- Default index for windows in frames: one of 'last', 'next' (for
    -- after current), or 'next-act' (for after current and anything with
    -- activity right after it).
    frame_default_index='next-act',

    -- Auto-unsqueeze transients/menus/queries.
    unsqueeze=true,

    -- Display notification tooltips for activity on hidden workspace.
    screen_notify=true,

    -- Automatically save layout on restart and exit.
    autosave_layout=true,

    -- Mouse focus mode; set to "sloppy" if you want the focus to follow the
    -- mouse, and to "disabled" otherwise.
    mousefocus="sloppy",
}

dopath("cfg_defaults")
dopath("look_monkey")

-- Load configuration of the Notion 'core'. Most bindings are here.
--dopath("cfg_notioncore")

-- Load some kludges to make apps behave better.
--dopath("cfg_kludges")

-- Define some layouts.
--dopath("cfg_layouts")

-- Load some modules. Bindings and other configuration specific to modules
-- are in the files cfg_modulename.lua.
--dopath("mod_query")
--dopath("mod_menu")
--dopath("mod_tiling")
--dopath("mod_statusbar")
--dopath("mod_dock")
--dopath("mod_sp")
--dopath("mod_notionflux")
--dopath("mod_xrandr")

os.execute("(emacs --daemon && emacsclient -c --no-wait)&")
