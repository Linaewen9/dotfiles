;;; General
(set-prefix-key (kbd "s-x"))

;;; Window management
;;;; Move window
(define-key *top-map* (kbd "s-S-Left") "exchange-direction left")
(define-key *top-map* (kbd "s-S-Right") "exchange-direction right")
(define-key *top-map* (kbd "s-S-Down") "exchange-direction down")
(define-key *top-map* (kbd "s-S-Up") "exchange-direction up")

;;;; Switch window
(define-key *root-map* (kbd "p") "prev-in-frame")
(define-key *root-map* (kbd "n") "next-in-frame")
(define-key *root-map* (kbd "s-b") "pull-from-windowlist")
(define-key *root-map* (kbd "b") "windowlist")
(define-key *root-map* (kbd "B") "frame-windowlist")
(define-key *root-map* (kbd "s-x") "other")

;;;; Split
(define-key *root-map* (kbd "0") "remove")
(define-key *root-map* (kbd "1") "only")
(define-key *root-map* (kbd "2") "vsplit")
(define-key *root-map* (kbd "3") "hsplit")

;;;; Select windows
(define-key *root-map* (kbd "&") "select-window-by-number 1")
(define-key *root-map* (kbd "eacute") "select-window-by-number 2")
(define-key *root-map* (kbd "\"") "select-window-by-number 3")
(define-key *root-map* (kbd "\'") "select-window-by-number 4")
(define-key *root-map* (kbd "(") "select-window-by-number 5")
(define-key *root-map* (kbd "-") "select-window-by-number Web")
(define-key *root-map* (kbd "egrave") "select-window-by-number 7")
(define-key *root-map* (kbd "_") "select-window-by-number 8")
(define-key *root-map* (kbd "ccedilla") "select-window-by-number 9")
(define-key *root-map* (kbd "agrave") "select-window-by-number 0")

;;;; Others
(define-key *root-map* (kbd "m") "tag-window")
(define-key *root-map* (kbd "M") "raise-tag")
(define-key *root-map* (kbd "t") "mark")
(define-key *root-map* (kbd "T") "pull-marked")
(define-key *root-map* (kbd "k") "delete-window")
(define-key *root-map* (kbd "C-k") "delete")
(define-key *root-map* (kbd "K") "kill")

;;; Media
(define-keysym #x1008ff02 "XF86MonBrightnessUp")
(define-keysym #x1008ff03 "XF86MonBrightnessDown")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "raise-volume")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "lower-volume")
(define-key *top-map* (kbd "XF86AudioMute") "toggle-mute-volume")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "raise-backlight")
(define-key *top-map* (kbd "XF86MonBrightnessDown") "lower-backlight")

;;; Tags
;;;; emacs
(define-key *root-map* (kbd "E") "tag-window emacs")
(define-key *root-map* (kbd "e") "raise-tag emacs")

;;;; chrome
(define-key *root-map* (kbd "Z") "tag-window chrome")
(define-key *root-map* (kbd "z") "raise-tag chrome")

;;; Shortcuts
(define-key *top-map* (kbd "s-e") "emacs")
(define-key *top-map* (kbd "s-z") "chrome")
(define-key *top-map* (kbd "s-t") "terminal-raise")
(define-key *root-map* (kbd "c") "terminal")
(define-key *top-map* (kbd "s-d") "exec dmenu_run")
(define-key *root-map* (kbd "DEL") "quit")
(define-key *root-map* (kbd "RET") "fullscreen")
(define-key *root-map* (kbd "l") "screen-saver")
(define-key *top-map* (kbd "s-m") "mode-line")
