(defconst my-savefile-dir (expand-file-name "~/.savefile"))
(setq auto-save-list-file-prefix
      (expand-file-name "auto-save-list/" my-savefile-dir))
(setq backup-directory-alist
      (expand-file-name "backups/" my-savefile-dir))
(setq custom-file
      (expand-file-name "custom-file.el" my-savefile-dir))
(require 'package)
(setq package-enable-at-startup nil)
(setq package-user-dir (expand-file-name "packages" my-savefile-dir))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

(use-package org :ensure t)
(use-package htmlize :ensure t)
;; to have things work correctly in batch-mode
(require 'font-lock)
(require 'cc-mode)
(require 'ox-org)
(c-after-font-lock-init)

(setq make-backup-files nil
      vc-handled-backends nil)

(setq org-export-default-language "en"
      org-export-html-extension "html"
      org-export-with-timestamps nil
      org-export-with-section-numbers nil
      org-export-with-tags 'not-in-toc
      org-export-skip-text-before-1st-heading nil
      org-export-with-sub-superscripts '{}
      org-export-with-LaTeX-fragments t
      org-export-with-archived-trees nil
      org-export-highlight-first-table-line t
      org-export-latex-listings-w-names nil
      org-html-head-include-default-style nil
      org-html-head ""
      org-export-htmlize-output-type 'css
      org-startup-folded nil
      org-export-allow-BIND t
      org-publish-list-skipped-files t
      org-publish-use-timestamps-flag t
      org-export-babel-evaluate nil
      org-confirm-babel-evaluate nil
      org-export-with-broken-links t)

;; re-export everything regardless of whether or not it's been modified
;; (setq org-publish-use-timestamps-flag nil)

(setq dir-base "/tmp")
(setq dir-html "/tmp")

(defun set-org-publish-project-alist ()
  "Set publishing projects for Wiki."
  (interactive)
  (setq org-publish-project-alist
	`(("project"
           :base-directory ,dir-base
	   :base-extension "org"
	   :html-extension "html"
	   :publishing-directory ,dir-html
	   :publishing-function (org-html-publish-to-html)
	   :recursive t
           :auto-sitemap nil
	   :section-numbers nil
	   :table-of-contents t
           :html-head "<link rel=\"stylesheet\" title=\"Standard\" href=\"http://orgmode.org/worg/style/worg.css\" type=\"text/css\" />
<link rel=\"alternate stylesheet\" title=\"Zenburn\" href=\"http://orgmode.org/worg/style/worg-zenburn.css\" type=\"text/css\" />
<link rel=\"alternate stylesheet\" title=\"Classic\" href=\"http://orgmode.org/worg/style/worg-classic.css\" type=\"text/css\" />"))))

(set-org-publish-project-alist)
