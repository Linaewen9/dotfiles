* STARTED Config
** REVIEW WAITING vimish                                    :WAITING:REVIEW:
:LOGBOOK:
- State "WAITING"    from "TODO"       [2018-02-18 Sun 19:05] \\
  Waiting for review from package owner
:END:
*** check problems from already existing vimish-fold [3/3]
**** CANCELED Add local-map                                     :CANCELED:
CLOSED: [2018-02-14 Wed 21:45]
:LOGBOOK:
- State "CANCELED"   from "HOLD"       [2018-02-14 Wed 21:45] \\
  too much overriding from minor modes
- State "HOLD"       from "DONE"       [2018-02-14 Wed 21:09] \\
  ca marche pas
:END:
**** DONE Add a minor mode map
CLOSED: [2018-02-14 Wed 22:18]
**** CANCELED add options ?                                     :CANCELED:
CLOSED: [2018-02-14 Wed 22:32]
:LOGBOOK:
- State "CANCELED"   from "HOLD"       [2018-02-14 Wed 22:32]
- State "HOLD"       from "TODO"       [2018-02-14 Wed 22:32] \\
  Dunno if needed
:END:
*** nested folds [4/4]
**** DONE Make refold refold only one necessary
CLOSED: [2018-02-14 Wed 23:02]
**** DONE Make vimish-fold--previous/next-nested-fold
CLOSED: [2018-02-15 Thu 23:52]
This should be functions that go to the previous / next folds even within a fold
**** DONE Fix vimish-fold-delete
CLOSED: [2018-02-16 Fri 15:54]
**** CANCELED Fringe color for nested                           :CANCELED:
CLOSED: [2018-02-16 Fri 15:54]
:LOGBOOK:
- State "CANCELED"   from "TODO"       [2018-02-16 Fri 15:54] \\
  Not necessary for myself, I'll pass if possible.
:END:
**** DONE Check all
CLOSED: [2018-02-18 Sun 19:00]
***** Docstrings
***** Clean code with new functions
**** check fold positionning
*** Conf [2/2]
**** DONE Binds
CLOSED: [2018-02-14 Wed 22:32]
#+BEGIN_SRC emacs-lisp
(defun mk/vimish-fold-map ()
  "Define key bindings for vimish-folds"
  (interactive)
  (let ((map vimish-fold-mode-map))
    (define-key map (kbd "C-<left>") 'vimish-fold-refold)
    (define-key map (kbd "C-<right>") 'vimish-fold-unfold)
    (define-key map (kbd "C-<up>") 'vimish-fold-previous-fold)
    (define-key map (kbd "C-<down>") 'vimish-fold-next-fold)
    ))
#+END_SRC
**** DONE advice goto-line
CLOSED: [2018-02-18 Sun 19:05]
#+BEGIN_SRC emacs-lisp
(defun mk/vimish-advice (&optional ARG PRED)
  (vimish-fold-unfold))

(advice-add 'goto-line :after #'mk/vimish-advice)
(advice-remove 'goto-line #'mk/vimish-advice)
#+END_SRC
*** DONE not in vimish, personal conf
CLOSED: [2018-02-18 Sun 19:05]
#+BEGIN_SRC emacs-lisp
(defun mk/vimish-advice (&optional ARG PRED)
  (vimish-fold-unfold))

(advice-add 'goto-line :after #'mk/vimish-advice)
(advice-remove 'goto-line #'mk/vimish-advice)
#+END_SRC
#+BEGIN_SRC emacs-lisp
(defun mk-test-prop (begin end)
  "blu"
  (interactive "r")
  (put-text-property begin end 'cursor-sensor-functions (list #'mk-bl)))
#+END_SRC
#+BEGIN_SRC emacs-lisp
(use-package vimish-fold :ensure t
  :config
  (load-file "~/Work/vimish-fold/vimish-fold.el")
  (setq vimish-fold-allow-nested t))

(defun mk/vimish-fold-buffer-regexp (beg end)
  (save-excursion
    (vimish-fold-delete-all)
    (goto-char (point-min))
    (let ((begs '()) (both (concat beg "\\|" end)) str-beg str-end)
      (while (re-search-forward both nil t)
        (setq str-beg (match-beginning 0))
        (setq str-end (match-end 0))
        (if (string-match-p
             (buffer-substring str-beg str-end) beg)
            (push str-beg begs)
          (if begs
              (progn
                (vimish-fold (pop begs) str-end)
                (goto-char (+ 1 str-end)))
            (error "Not all folds have a match"))))))
  (vimish-fold-refold-all)
  (goto-char (point-min))
  (re-search-forward beg)
  (beginning-of-line))

(defun mk/vimish-fold-map()
  (interactive)
  (let ((map vimish-fold-folded-keymap))
    (define-key map (kbd "C-<left>") 'vimish-fold-refold)
    (define-key map (kbd "C-<right>") 'vimish-fold-unfold)
    (define-key map (kbd "C-<up>") 'vimish-fold-previous-existing-fold)
    (define-key map (kbd "C-<down>") 'vimish-fold-next-fold)
    )
  (let ((map vimish-fold-unfolded-keymap))
    (define-key map (kbd "C-<left>") 'vimish-fold-refold)
    (define-key map (kbd "C-<right>") 'vimish-fold-unfold)
    (define-key map (kbd "C-<up>") 'vimish-fold-previous-existing-fold)
    (define-key map (kbd "C-<down>") 'vimish-fold-next-fold)
    ))
#+END_SRC
** DONE No more dozen of files
CLOSED: [2018-02-18 Sun 23:09]
- State "DONE"       from "CANCELED"   [2018-02-18 Sun 23:10]
- State "DONE"       from "TODO"       [2018-02-18 Sun 23:09]
- State "DONE"       from "TODO"       [2018-02-18 Sun 23:09]
*** Migrate all in mk.org
** DONE Org mode rework
** DONE helm rework
:LOGBOOK:
- State "TODO"       from "CANCELED"   [2018-02-18 Sun 23:06] \\
  
:END:
** DONE Flycheck
CLOSED: [2018-02-18 Sun 19:55]
*** DONE popup info
CLOSED: [2018-02-18 Sun 19:37]
*** DONE Modeline
CLOSED: [2018-02-18 Sun 19:54]
** DONE Mode line rework
CLOSED: [2018-02-18 Sun 19:55]
I want the git branch and status in that fucking modeline !
** Various [4/12]
*** STARTED Load each module in a dedicated block
*** TODO Warning emacs < 25.2 which fixes a major security bug
*** TODO Customize all necessary directories
**** TODO See for backup-directory-alist
I removed it since it was not correctly set.
*** TODO Do I really have to load hs-binds before anything else ? Why ?
*** TODO Rework intersec conf to be nice and clean.
*** TODO Modules loading options at top of mk.org
*** TODO Function for remake conf
*** TODO Function for reload con
*** CANCELED Load mu4e normally and make it check the directories itself :CANCELED:
CLOSED: [2018-02-22 Thu 23:21]
- State "CANCELED"   from "TODO"       [2018-02-22 Thu 23:21] \\
  It's good in the current state, having the check in the general file
  is a good thing.
*** DONE line and theme should be a common appearance package
CLOSED: [2018-02-22 Thu 23:22]
- State "DONE"       from "TODO"       [2018-02-22 Thu 23:22]
or at least sub org point
*** DONE Clear settings section
CLOSED: [2018-02-22 Thu 23:23]
- State "DONE"       from "TODO"       [2018-02-22 Thu 23:23]
*** CANCELED function for org src block                          :CANCELED:
CLOSED: [2018-02-22 Thu 23:23]
- State "CANCELED"   from "TODO"       [2018-02-22 Thu 23:23] \\
  It already exists (see org function related to block / dblocks)
Create a interactive function for generating org src blocksf
** STARTED Fully document config
* Other configs
** See [[http://pages.sachachua.com][A good config]]
*** erefactor quick
** EOS
