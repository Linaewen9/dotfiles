import sys
import os

commands = dict()
commands['Keyboard: US'] = 'bash ~/.scripts/mk-tools.sh X us'
commands['Keyboard: FR'] = 'bash ~/.scripts/mk-tools.sh X fr'
commands['Screenshot'] = "pkill rofi && scrot '%Y-%m-%d-%T_$wx$h_scrot.png' -e 'mv $f ~/Pictures/Screenshots/' -s"
commands['Volume: Up'] = 'bash ~/.scripts/mk-tools.sh v u'
commands['Volume: Down'] = 'bash ~/.scripts/mk-tools.sh v d'
commands['Volume: Toggle mute'] = 'bash ~/.scripts/mk-tools.sh v t'
commands['Light: Up'] = 'bash ~/.scripts/mk-tools.sh l u'
commands['Light: Down'] = 'bash ~/.scripts/mk-tools.sh l d'
commands['Lock'] = 'bash ~/.scripts/mk-tools.sh lock'
commands['Keyboard: Clean'] = 'setxkbmap -option'
commands['Test notification'] = 'notify-send "Hello !" "rofi"'
commands['Rofi: Run'] = 'pkill rofi && rofi -show run'
commands['Rofi: Window'] = 'pkill rofi && rofi -show window'
commands['Rofi: Keys'] = 'pkill rofi && rofi -show keys'
commands['Quit'] = 'pkill rofi'

arg = ''

if sys.argv[1:]:
    arg = ' '.join(sys.argv[1:])

    if arg in commands:
        os.system(commands[arg])
        print(arg)

for key in commands:
    if key != arg:
        print(key)
