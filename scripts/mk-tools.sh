#!/usr/bin/bash

# {{{ Logs

LOG_FILE="/tmp/mk-tools.log"
LOG_TAG=""

log_tag() {
    LOG_TAG=$1
}

log() {
    if [ "$1" != "" ]; then
        if [ "$LOG_TAG" != "" ]; then
            echo -n "[$LOG_TAG] " >> $LOG_FILE
        fi
        echo "$@" >> $LOG_FILE
    fi
}

log_hostname() {
    echo -n "[$(hostname)] " >> $LOG_FILE
}

log_date() {
    echo -n "[$(date)] " >> $LOG_FILE
    log $@
}

log_dh() {
    log_date
    log_hostname
    log $@
}

# }}}
# {{{ Permissions

hostError() {
    log "This command is not available to the current host"
    exit 0
}

doNotAllow() {
    for host in "$@"
    do
        if [ "$(hostname)" == "$host" ]; then
            hostError
        fi
    done
}

allowOnly() {
    allowed=0
    for host in "$@"
    do
        if [ "$(hostname)" == "$host" ]; then
            allowed=1
        fi
    done

    if [ $allowed -eq 0 ]; then
        hostError
    fi
}

# }}}
# {{{ Volume

volumeUp() {
    amixer -q sset 'Master' 1%+
}

volumeDown() {
    amixer -q sset 'Master' 1%-
}

volumeToggle() {
    amixer -q sset 'Master' toggle
}

# }}}
# {{{ Light

lightUp() {
    light -A 10
}

lightDown() {
    light -U 10
}

# }}}
# {{{ Lock screen

_lock() {
    case "$(hostname)" in
        "mandarine")
            if [ -z $(pgrep convert) ]
            then
                (betterlockscreen -u $LOCK_DIR/current_lockscreen.jpg -r 1920x1080 2>>/dev/null && notify-send "Lock screen" "$(date) Success update" -u low || notify-send "Lock screen" "$(date) Failed update" -u critical) &
            fi
            betterlockscreen -l
            ;;
        *)
            log "Lock directory:" "$LOCK_DIR"

            # Find a random image from the lock dir
            p=$(find "$LOCK_DIR" -type f | sort -R | head -n 1)

            # Only want png file
            delete=0
            if [ -n "$p" ] && echo "$p" | grep -q '.jpg$'; then
                log "Format image to PNG and mark deletion."
                delete=1
                no_ext=$(echo $p | sed 's/\.[^.]*$//')
                convert $no_ext.jpg $no_ext.png
                p=$no_ext.png
            fi

            if [ "$p" == "" ]; then
                log "No image found, lock without image !"
                i3lock -n -t -e -c 000000 2>>/tmp/i3lock_debug.log
            else
                log "Locking with image:" $p
                i3lock -n -t -e -i $p 2>>/tmp/i3lock_debug.log
            fi

            if [ $delete == 1 ]; then
                rm $p
            fi
            ;;
    esac
}

lock() {
    log_tag "Lock"
    log_dh "Start"

    # suspend dusnt message display
    pkill -u "$USER" -USR1 dunst

    case "$(hostname)" in
        "mandarine")
            LOCK_DIR="/home/Misc/Pictures/"
            ;;
        *)
            LOCK_DIR=~/Pictures/locks/
            ;;
    esac

    _lock

    # resume dusnt message display
    pkill -u "$USER" -USR2 dunst

    log_dh "End"
    log_tag ""
}

# }}}
# {{{ Screenshot

screenshot() {
    scrot '%Y-%m-%d-%T_$wx$h_scrot.png' -e 'mv $f ~/Misc/Pictures/screenshots/'
}

# }}}
# {{{ X settings

_xKeyReset() {
    log "Reset settings"

    xset dpms 0 0 0
    xset s off
    xset r rate 400 60
    setxkbmap -option

    log "Reset done"
}

xKeyQwerty() {
    log_tag "X settings"
    log_dh "Switch to Qwerty"

    _xKeyReset

    setxkbmap us
    setxkbmap -option caps:ctrl_modifier
    setxkbmap -option compose:ralt

    log_dh "Done"
    log_tag ""
}

xKeyAzerty() {
    log_tag "X settings"
    log_dh "Switch to Azerty"

    _xKeyReset
    setxkbmap fr
    setxkbmap -option caps:ctrl_modifier

    log_dh "Done"
    log_tag ""
}

# }}}
# {{{ Notifications

notifStart() {
    dunst -config ~/.i3/dunst/dunstrc
}

notifKill() {
    pkill -9 dunst
}

notifRestart() {
    notifKill
    notifStart
}

# }}}

case "$1" in
    # {{{ Volume

    "volume"|"v")
        case "$2" in
            "up"|"u")
                volumeUp
                ;;
            "down"|"d")
                volumeDown
                ;;
            "toggle"|"t")
                volumeToggle
                ;;
            *)
                echo "Unkown volume option:" $@
                ;;
        esac
        ;;

    # }}}
    # {{{ Light / Screen brightness

    "light"|"l")
        doNotAllow "dracula"

        case "$2" in
            "up"|"u")
                lightUp
                ;;
            "down"|"d")
                lightDown
                ;;
            *)
                echo "Unkown light option:" $@
                ;;
        esac
        ;;

    # }}}
    # {{{ Lock screen

    "lock")
        lock
        ;;

    # }}}
    # {{{ Screenshot

    "screenshot")
        screenshot
        ;;

    # }}}
    # {{{ X settings

    "X"|"x")
        case "$2" in
            "qwerty" | "us")
                xKeyQwerty
                ;;
            "azerty" | "fr")
                xKeyAzerty
                ;;
            *)
                echo "Unkown X option:" $@
                ;;
        esac
        ;;

    # }}}
    # {{{ Notifications

    "dunst")
        case "$2" in
            "start"|"s")
                notifStart
                ;;
            "kill"|"k")
                notifKill
                ;;
            "restart"|"r")
                notifRestart
                ;;
            *)
                echo "Unkown notifications option:" $@
                ;;
        esac
        ;;

    # }}}
    # {{{ Log
    "log")
        shift
        case "$1" in
            "date")
                shift
                log_date $@
                ;;
            "hostname")
                shift
                log_hostname
                ;;
            "dh")
                shift
                log_dh $@
                ;;
            *)
                log $@
                ;;
        esac
        ;;
    # }}}
    # {{{ Shell
    "shell")
        if [ $(command -v "sakura") ]
        then
            sakura -e zsh
        else
            urxvt
        fi
        ;;
    # }}}
    # {{{ Arch packages
    "arch")
        shift
        case "$1" in
            "dump-packages")
                pacman -Qqe > ~/.dotfiles/packages.list
                ;;
            "info-packages")
                for i in $(pacman -Qqe); do
                    pacman -Qi $i
                    read -p "Press any key to continue... " -n1 -s
                done
                ;;
        esac
        ;;
    # }}}
    *)
        echo "Unknown option:" $1
        ;;
esac
