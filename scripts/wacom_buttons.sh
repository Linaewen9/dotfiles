#!/usr/bin/env sh

# Cancel
xsetwacom --set "Wacom Intuos Pro M Pad pad" Button 8 "key +ctrl z -ctrl"

# Copy
xsetwacom --set "Wacom Intuos Pro M Pad pad" Button 9 "key +ctrl c -ctrl"

# Cut
xsetwacom --set "Wacom Intuos Pro M Pad pad" Button 10 "key +ctrl x -ctrl"

# Paste
xsetwacom --set "Wacom Intuos Pro M Pad pad" Button 11 "key +ctrl v -ctrl"

# Save
xsetwacom --set "Wacom Intuos Pro M Pad pad" Button 12 "key +ctrl s -ctrl"
